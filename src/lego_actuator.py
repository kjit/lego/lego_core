#!/usr/bin/env python
import rospy
import tf.transformations
from geometry_msgs.msg import Twist, Pose, Point, Quaternion, Vector3
from nav_msgs.msg import Odometry
import math
import brickpi3 # import the BrickPi3 drivers

R = 0.028 #Large lego wheel
L = 0.1575
BP = brickpi3.BrickPi3()
th = 0.0
x = 0.0
y = 0.0

def callback(msg):
    global R,L ,BP, th, x, y  

    rospy.loginfo("Received a /cmd_vel message!")
    rospy.loginfo("Linear Components: [%f, %f, %f]"%(msg.linear.x, msg.linear.y, msg.linear.z))
    rospy.loginfo("Angular Components: [%f, %f, %f]"%(msg.angular.x, msg.angular.y, msg.angular.z))

    v = msg.linear.x
    '''if v == 0:
        th = 0.0
        x = 0.0
        y = 0.0
        print('megtortent')
    '''
    v_r = (v-msg.angular.z/(L/4)/250)/R*5
    v_l = (v+msg.angular.z/(L/4)/250)/R*5
    #print(v)
    rospy.loginfo(" vl, vr: [ %d, %d]"%(v_l,v_r))

    # Set wheel speeds with BrickPi
    BP.set_motor_power(BP.PORT_C,v_r)
    BP.set_motor_power(BP.PORT_B,v_l)

def listener():
    global R,L ,BP, th, x, y
    x = 0.0
    y = 0.0
    th = 0.0
    
    vx = 0.1
    vy = -0.1
    vth = 0.1
    rospy.init_node('lego_robot')
    rospy.Subscriber("/cmd_vel", Twist, callback)
    odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
    odom_broadcaster = tf.TransformBroadcaster()
    r = rospy.Rate(10)
    current_time = rospy.Time.now()
    last_time = rospy.Time.now()
    BP.set_motor_power(BP.PORT_C, 0)
    BP.set_motor_power(BP.PORT_B, 0)    
    odom_tick_r = BP.get_motor_encoder(BP.PORT_C)
    odom_tick_l = BP.get_motor_encoder(BP.PORT_B)
    odom_tick_r_prev = odom_tick_r
    odom_tick_l_prev = odom_tick_l
    while not rospy.is_shutdown():
        #print('while start')
        current_time = rospy.Time.now()
        dt = (current_time - last_time).to_sec()
        #print('right:', odom_tick_r)
        #print('left:', odom_tick_l)
        odom_tick_r = BP.get_motor_encoder(BP.PORT_C)
        odom_tick_l = BP.get_motor_encoder(BP.PORT_B)
        dtick_r = odom_tick_r - odom_tick_r_prev
        dtick_l = odom_tick_l - odom_tick_l_prev
        Dr = 2*math.pi*R*dtick_r/360
        Dl = 2*math.pi*R*dtick_l/360
        #rospy.loginfo("Dl, Dr: [%f, %f]"%(Dl,Dr))
        Dc = (Dr+Dl)/2
        #print('Dc')
        delta_x = Dc*math.cos(th)
        delta_y = Dc*math.sin(th)
        delta_th = (Dl-Dr)/L
        #print('Delta')
        x += delta_x
        y += delta_y
        #print('x:', x)
        #print('y:', y)
        vx = delta_x/dt
        vy = delta_y/dt

        th += delta_th
        vth = delta_th/dt
        #print('th:', th)
        
        # since all odometry is 6DOF we'll need a quaternion created from yaw
        odom_quat = tf.transformations.quaternion_from_euler(0, 0, th)
        #print('quat')
        # first, we'll publish the transform over tf
        odom_broadcaster.sendTransform(
                (x, y, 0.),
                odom_quat,
                current_time,
                "base_link",
                "odom"
        )
        #print('sendtransfrom')
        # next, we'll publish the odometry message over ROS
        odom = Odometry()
        odom.header.stamp = current_time
        odom.header.frame_id = "odom"
        # set the position
        odom.pose.pose = Pose(Point(x, y, 0.), Quaternion(*odom_quat))

        # set the velocity
        odom.child_frame_id = "base_link"
        odom.twist.twist = Twist(Vector3(vx, vy, 0), Vector3(0, 0, vth))

        # publish the message
        odom_pub.publish(odom)
        #print('publish odom')
        last_time = current_time
        odom_tick_r_prev = odom_tick_r
        odom_tick_l_prev = odom_tick_l
        r.sleep()

if __name__ == '__main__':
    listener()

    
